onLoadName = "ZEUS - GAME MASTER";
loadScreen = "\a3\Missions_F_EPA\data\img\Showcase_Fixed_Wings_overview_CA.paa";
overviewPicture = "\a3\Missions_F_EPA\data\img\Showcase_Fixed_Wings_overview_CA.paa";
overviewText = "Zeus Game Master";
dev = "[]";
author = "[]";
respawnDelay = 5;

corpseManagerMode = 1;
corpseLimit = 15;
corpseRemovalMinTime = 300;
corpseRemovalMaxTime = 1800;
wreckManagerMode = 1;
wreckLimit = 5;
wreckRemovalMinTime = 30;
wreckRemovalMaxTime = 60;
minPlayerDistance = 50;