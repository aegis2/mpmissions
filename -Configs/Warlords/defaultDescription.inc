onLoadMission = $STR_A3_WL_modRules;
onLoadName = "WARLORDS - EXPANDED";
loadScreen = "A3\Data_F_Warlords\data\wl_logo_ca.paa";
overviewPicture = "A3\Data_F_Warlords\data\wl_logo_ca.paa";
overviewText = $STR_A3_WL_modRules;
dev = "[]";
author = "[]";
respawn = "BASE";
respawnDelay = 5;

corpseManagerMode = 1;
corpseLimit = 15;
corpseRemovalMinTime = 300;
corpseRemovalMaxTime = 1800;
wreckManagerMode = 1;
wreckLimit = 5;
wreckRemovalMinTime = 30;
wreckRemovalMaxTime = 60;
minPlayerDistance = 50;

WLAIRequisitonBlacklist[] = {"B_crew_F", "B_Helipilot_F", "B_Pilot_F", "O_crew_F", "O_Helipilot_F", "O_Pilot_F", "B_T_crew_F", "B_T_Helipilot_F", "B_T_Pilot_F", "O_T_crew_F", "O_T_Helipilot_F", "O_T_Pilot_F"};